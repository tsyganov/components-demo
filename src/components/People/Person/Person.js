import React from 'react';
import './Person.css';

const Person = props => {
  return (
    <div className="Person">
      <h1 onClick={props.click}>{props.name}</h1>
      <p>Age: {props.age}</p>
      <p onClick={props.remove}>{props.children}</p>
      <p><input type="text" value={props.name} onChange={props.change} /></p>
    </div>
  );
};

export default Person;