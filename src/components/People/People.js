import React from 'react';
import Person from "./Person/Person";

const People = props => {
  // props = {'people': [], 'click': function(), 'change': function(), 'remove': function()}
  // <People people="" click="" change="" remove="" />
  return props.people.map((person) => {
    return <Person
      key={person.id}
      name={person.name}
      age={person.age}
      click={() => props.click(person.id)}
      change={(event) => props.change(event, person.id)}
      remove={() => props.remove(person.id)}
    >
      {person.hobbies}
    </Person>
  })
};

export default People;