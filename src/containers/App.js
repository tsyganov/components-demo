import React, {Component} from 'react';
import './App.css';
import People from "../components/People/People";

class App extends Component {
  state = {
    people: [
      {name: 'Dmitrii', age: 29, hobbies: 'Hobby: Video Games', id: 'd1'}, // 0
      {name: 'John', age: 30, hobbies: 'Likes drinking alone', id: 'j1'},     // 1
      {name: 'Jaxck', age: 69, hobbies: 'Collects post stamps', id: 'j2'},     // 2
      {name: 'Haru-kun', age: 19, hobbies: 'Otaku desu!', id: 'h99'}     // 3
    ],
    showPeople: false
  };

  changeName = (event, id) => {
    const index = this.state.people.findIndex(p => p.id === id);
    const person = {...this.state.people[index]};
    person.name = event.target.value;

    const people = [...this.state.people];
    people[index] = person;

    this.setState({people});
  };

  increaseAge = (id) => {
    const index = this.state.people.findIndex(p => p.id === id);
    const person = {...this.state.people[index]};
    person.age++;

    const people = [...this.state.people];
    people[index] = person;

    this.setState({people});
  };

  togglePeople = () => {
    const showPeople = !this.state.showPeople;

    this.setState({showPeople});
  };

  removePerson = (id) => {
    const index = this.state.people.findIndex(p => p.id === id);
    const people = [...this.state.people];
    people.splice(index, 1);

    this.setState({people});
  };

  render() {
    let people = null;
    let buttonClasses = ['Button'];
    let counterStyle = {color: 'green'};

    if (this.state.showPeople) {
      people = <People
        people={this.state.people}
        click={this.increaseAge}
        change={this.changeName}
        remove={this.removePerson}
      />;

      buttonClasses.push('Red');
    }

    if (this.state.people.length <= 2) {
      counterStyle.color = 'red';
    }

    if (this.state.people.length <= 1) {
      counterStyle.fontWeight = 'bold';
    }

    return (
      <div className="App">
        <div style={{marginTop: '20px'}}>
          <button className={buttonClasses.join(' ')} onClick={this.togglePeople}>Toggle people</button>
        </div>
        <p style={counterStyle}>
          Total people: {this.state.people.length}
        </p>
        {people}
      </div>
    );
  }
}

export default App;
